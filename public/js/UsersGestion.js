// Firebase CDN'S

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// FirebaseConfig

const firebaseConfig = {
  apiKey: "AIzaSyA4mh-2Kc2FSy9DNdin60gUKz_UVvrINnk",
  authDomain: "dungueons-and-rol.firebaseapp.com",
  databaseURL: "https://dungueons-and-rol-default-rtdb.firebaseio.com/", 
  projectId: "dungueons-and-rol",
  storageBucket: "dungueons-and-rol.appspot.com",
  messagingSenderId: "547287557437",
  appId: "1:547287557437:web:582c2ca2a9396a85dfd154",
  measurementId: "G-6HDLRXYJ8P"
};

// Firebase consts

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();

// HTML references

var correoRef = document.getElementById("direccionCorreoId");
var passRef = document.getElementById("passwordId");
var CreateCorreoRef = document.getElementById("CDireccionCorreoId");
var CreatePassRef = document.getElementById("CPasswordId");
var CreatePass2Ref = document.getElementById("CPassword2Id");
var CreateNameRef = document.getElementById("CNameId");
var CreateCityRef = document.getElementById("CCityId");
var buttonRef = document.getElementById("altaButtonId");
var ingresarRef = document.getElementById("ingresarButtonId");

// EventsListeners

buttonRef.addEventListener("click", altaUsuario);
ingresarRef.addEventListener("click", logIn);

//Functions

// Create User

function altaUsuario()
{
    if((CreateCorreoRef.value.trim() != '') && (CreatePassRef.value.trim() != '') && (CreatePass2Ref.value.trim() != '') && (CreateNameRef.value.trim() != '') && (CreateCityRef.value.trim() != ''))
    {

        if (CreatePass2Ref.value != CreatePassRef.value){
                alert("Las contraseñas no coinciden")
            }
        else{
            createUserWithEmailAndPassword(auth, CreateCorreoRef.value, CreatePassRef.value)
            .then((userCredential) => {
                const user = userCredential.user;
                DataBaseUser(user.uid);
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                if(errorCode == 'auth/email-already-in-use'){
                    alert("El correo electronico ya se encuentra en uso");
                }
            });
        }
        
    }
    else
    {
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    
}

// Create user copy in database

function DataBaseUser(uid)
{
    let email = CreateCorreoRef.value
    let nombre = CreateNameRef.value
    let ciudad = CreateCityRef.value
    const UserData = {
        uid:  uid,
        Name: nombre,
        Email: email, 
        city: ciudad,
      };
    CreateCorreoRef.value = ""
    CreateNameRef.value = ""
    CreateCityRef.value = ""
    CreatePass2Ref.value = ""
    CreatePassRef.value = ""
    console.log(UserData)
    console.log(uid)

    set(ref(database, "users/" + uid), {
        UserData
    })
}

// LogIn

function logIn ()
{

    if((correoRef.value != '') && (passRef.value != '')){

        signInWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "../index.html";

        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revise que los campos de usuario y contraseña esten completos.");
    }    
}
