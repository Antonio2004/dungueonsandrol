import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, child, ref, push, update, set, get, remove} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyA4mh-2Kc2FSy9DNdin60gUKz_UVvrINnk",
    authDomain: "dungueons-and-rol.firebaseapp.com",
    databaseURL: "https://dungueons-and-rol-default-rtdb.firebaseio.com/", 
    projectId: "dungueons-and-rol",
    storageBucket: "dungueons-and-rol.appspot.com",
    messagingSenderId: "547287557437",
    appId: "1:547287557437:web:582c2ca2a9396a85dfd154",
    measurementId: "G-6HDLRXYJ8P"
  };

const app = initializeApp(firebaseConfig);

//Referencias al HTML

var Section = document.getElementById("postID");
var textoRef = document.getElementById("textInputID");
var tituloRef = document.getElementById("titleInputID");
var botonRef = document.getElementById("sendPostBtnID");
var usuarioRef = document.getElementById("UserLogId");
var newPostID = document.getElementById("newPostID");
var botonEditRef = document.getElementById("buttonEditId");
var btnLogIn = document.getElementById("btnlogin");
var btnLogOut = document.getElementById("btnlogout");

// var 

var UsuarioData;
var Posts;
var change = false;
const auth = getAuth();
const database = getDatabase();
const user = auth.currentUser;
var UserID;
var thekey;

//Events
botonEditRef.addEventListener("click", FinishEdit);
btnLogOut.addEventListener("click", LogOut);
botonRef.addEventListener("click", writeNewPost);

// Hide Buttons

botonEditRef.style.display = "none";

// Check user log

onAuthStateChanged(auth, (user) => {
  if (user) {

    console.log("Usuario logeado")
    btnLogIn.style.display = "none";
    btnLogOut.style.display = "block";
    newPostID.style.display = "flex",
    
    get(ref(database, "users/" + user.uid ) ).then((snapshot) => {
      if (snapshot.exists()) {

        UsuarioData = snapshot.val().UserData
        UserID = UsuarioData.uid
        usuarioRef.appendChild(document.createTextNode(UsuarioData.Name + " "))
        
        GetDataBase();

      }
    }).catch((error) => {
      console.error(error);
    });  
}
else{

  btnLogIn.style.display = "block";
  btnLogOut.style.display = "none";
  
  GetDataBase()

}});

// LogOut

function LogOut(){

  auth.signOut()
  location.reload(true)

}

// Get Data

function GetDataBase(){
  get(ref(database, "posts/")).then((snapshot) => {
    if (snapshot.exists()) {
      
      Posts = snapshot.val()
      
			for (const i in Posts){

        console.log(Posts[i].uid)
        let uid = Posts[i].uid
        Post(Posts[i].title, Posts[i].body, Posts[i].author, Posts[i].city, i,uid)   
			}
    }
  }).catch((error) => {
    console.error(error);
  });
}

function Post(titulo, cuerpo, nombre, ciudad, key, Userid){

  let tag = document.createElement("div");
  
  tag.classList.add('post');
  
  let title = document.createElement("p");
	title.classList.add("title");
  let text = document.createTextNode(cuerpo);
  title.innerHTML = titulo + "[Publicado por " + nombre + " de " + ciudad + "]";
  tag.appendChild(title);
  tag.appendChild(text);
  console.log(UserID);

  if (Userid == UserID){
    let botonE = document.createElement("i");
    botonE.classList.add('bi' , 'bi-pencil-fill');
    let botonC = document.createElement("i");
    botonC.classList.add('bi' , 'bi-trash3');

    // botonE.appendChild(btextE)
    // botonC.appendChild(btextC)
    
    tag.appendChild(botonE)
    tag.appendChild(botonC)

    botonE.addEventListener("click", (e) => {
      
      EditPost(key);
    })
    botonC.addEventListener("click", (e) => {
      ClearPost(key);
      console.log("2");
    });
  }
    Section.prepend(tag);
  }

// Upload Data

function writeNewPost(){
  // A post entry.
  if( textoRef.value.trim() != "" && tituloRef.value.trim() != "" ){
  let postData = {
    author: UsuarioData.Name,
    body: textoRef.value,
    title: tituloRef.value,
    city: UsuarioData.city,
    uid: UsuarioData.uid,
  };

  let newPostKey = push(child(ref(database), 'posts')).key;
  Post(tituloRef.value, textoRef.value, UsuarioData.Name, UsuarioData.city, newPostKey, UsuarioData.uid)
  tituloRef.value = ""
  textoRef.value = ""
  const updates = {};
  updates['/posts/' + newPostKey] = postData;

  return update(ref(database), updates)
  }
};

// Edit Data

function EditPost(Postkey)
{
		botonEditRef.style.display = "block";
    thekey = Postkey;
    get(ref(database, "posts/" + thekey)).then((snapshot) => {
      if (snapshot.exists()) {
        console.log(snapshot.val());
        Posts = snapshot.val();
        textoRef.value = Posts.body;
        tituloRef.value = Posts.title; 
        botonRef.style.display = "none";
				botonEditRef.style.display = "block";
        change = true;
      }
    }).catch((error) => {
      console.error(error);
    });
}

function FinishEdit(){
  remove(ref(database, "posts/" + thekey))
  writeNewPost()
  location.reload(true)
}

function ClearPost(Postkey){
	if(confirm(" ¿Estas seguro que quieres eliminar el post?")){
  remove(ref(database, "posts/" + Postkey));
  location.reload(true)
	}
}